﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Obstacle : MonoBehaviour {

    Mov Mov;

	private void Start () {
        Mov = GameObject.FindObjectOfType<Mov>();
	}

    private void OnCollisionEnter (Collision collision)
    {
        if (collision.gameObject.tag= "Player") {
            
            Mov.Die();
        }
    }

    private void Update () {
	
	}
}