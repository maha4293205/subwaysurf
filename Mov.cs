using UnityEngine;
using UnityEngine.SceneManagement;

public class Mov : MonoBehaviour {

    bool alive = true;

    public float speed = 5;
    [SerializeField] Rigidbody rb;

    float horizontalInput;
    bool isDragging = false;
    Vector2 dragStartPosition;
    float width;

    void Start()
    {
        width = (float)Screen.width ;
    }
    private void FixedUpdate ()
    {
        if (!alive) return;
        Vector3 forwardMove = transform.forward * speed * Time.fixedDeltaTime;
        Vector3 horizontalMove = transform.right * horizontalInput * speed * Time.fixedDeltaTime;
        rb.MovePosition(rb.position + forwardMove + horizontalMove);
    }

    private void Update () {
        HandleTouchInput();
        if (transform.position.y < -5) {
            Die();
        }
	}

    void HandleTouchInput()
{
    if (Input.touchCount > 0) {
        Touch touch = Input.GetTouch(0);
        switch (touch.phase) {
            case TouchPhase.Began:
                isDragging = true;
                dragStartPosition = touch.position;
                break;
            case TouchPhase.Moved:
                if (isDragging) {
                    horizontalInput = (touch.position.x - dragStartPosition.x) /width * 5f; 
                }
                break;
            case TouchPhase.Ended:
                isDragging = false;
                horizontalInput = 0f;
                break;
        }
    }
}
public void Die ()
    {
        alive = false;
        // Restart the game
        Invoke("Restart", 2);
    }

    void Restart ()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }


}